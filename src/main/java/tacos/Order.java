package tacos;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Order {

    private Long id;

    private Date placedAt;

    @JsonProperty("deliveryName")
    @Size(max = 50, message = "must less than {max} characters")
    @NotBlank
    private String name;

    @JsonProperty("deliveryStreet")
    @Size(max = 50, message = "must less than {max} characters")
    @NotBlank
    private String street;

    @JsonProperty("deliveryCity")
    @Size(max = 50, message = "must less than {max} characters")
    @NotBlank
    private String city;

    @JsonProperty("deliveryState")
    @Size(max = 2, message = "must less than {max} characters")
    @NotBlank
    private String state;

    @JsonProperty("deliveryZip")
    @Size(max = 10, message = "must less than {max} characters")
    @NotBlank
    private String zip;

    @JsonProperty("ccNumber")
    @Digits(integer = 16, fraction = 0, message="Invalid cc number")
    @NotBlank(message = "Not a valid credit card number")
    private String ccNumber;

    @JsonProperty("ccExpiration")
    @Pattern(regexp = "^(0[1-9]|1[0-2])([\\/])([1-9][0-9])$", message = "Must be formatted MM/YY")
    private String ccExpiration;

    @JsonProperty("ccCVV")
    @Digits(integer = 3, fraction = 0, message="Invalid CVV")
    private String ccCVV;

    private List<Taco> tacos;

    public Order() {
        this.tacos = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getCcNumber() {
        return ccNumber;
    }

    public void setCcNumber(String ccNumber) {
        this.ccNumber = ccNumber;
    }

    public String getCcExpiration() {
        return ccExpiration;
    }

    public void setCcExpiration(String ccExpiration) {
        this.ccExpiration = ccExpiration;
    }

    public String getCcCVV() {
        return ccCVV;
    }

    public void setCcCVV(String ccCVV) {
        this.ccCVV = ccCVV;
    }

    public Long getId() {
        return id;
    }

    public List<Taco> getTacos() {
        return tacos;
    }

    public void setTacos(List<Taco> tacos) {
        this.tacos = tacos;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getPlacedAt() {
        return placedAt;
    }

    public void setPlacedAt(Date placedAt) {
        this.placedAt = placedAt;
    }

    @Override
    public String toString() {
        return "Order{" +
                "name='" + name + '\'' +
                ", street='" + street + '\'' +
                ", city='" + city + '\'' +
                ", state='" + state + '\'' +
                ", zip='" + zip + '\'' +
                ", ccNumber='" + ccNumber + '\'' +
                ", ccExpiration='" + ccExpiration + '\'' +
                ", ccCVV='" + ccCVV + '\'' +
                '}';
    }

    public void addDesign(Taco saved) {
        tacos.add(saved);
    }
}
